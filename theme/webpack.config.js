const path = require('path');
const argv = require('yargs').argv;
const webpack = require('webpack');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserJSPlugin = require('terser-webpack-plugin');
const OptimizeCSSAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports = {
    // watch: true,
    // watchOptions: {
    //     aggregateTimeout: 600
    // },
    mode: argv.production ? 'production' : 'development',
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /(node_modules)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env'],
                        plugins: ['@babel/plugin-transform-runtime'],
                    }
                }
            },
            {
                test: /\.html$/, 
                use: {
                    loader: "underscore-template-loader" 
                }
            },
            {
                test: /\.((sa)|(s?c))ss$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    'css-loader',
                    'sass-loader'
                ]
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]'
                        }
                    }
                ]
            }
        ]
    },
//     /* Uncomment to use external jQuery as $ */
    externals: {
        jquery: 'jQuery'
    },
    plugins: [
        /* Uncomment to use internal jQuery as $ */
        // new webpack.ProvidePlugin({
        //     $: 'jquery',
        //     jQuery: 'jquery'
        // })
        new MiniCssExtractPlugin({
            filename: '[name].css'
        }),
    ],
    resolve: {
        alias: {
            Styles: path.resolve(__dirname, 'src/styles/'),
            node_modules: path.resolve(__dirname, 'node_modules'),
        }
    }
};